<?php

use Illuminate\Database\Seeder;

class PairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Pair::firstOrCreate([
            'name' => "BTC-EUR"
        ]);

        \App\Models\Pair::firstOrCreate([
            'name' => "BTC-USD"
        ]);

        \App\Models\Pair::firstOrCreate([
            'name' => "LTC-EUR"
        ]);

        \App\Models\Pair::firstOrCreate([
            'name' => "LTC-USD"
        ]);
    }
}
