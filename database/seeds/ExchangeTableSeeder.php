<?php

use Illuminate\Database\Seeder;

class ExchangeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Exchange::firstOrCreate([
            'name' => "Gdax",
            'code' => "GDAX",
            'country' => "USA",
            'url' => "https://gdax.com/"
        ]);

        \App\Models\Exchange::firstOrCreate([
            'name' => "Bifinex",
            'code' => "BITF",
            'country' => "USA",
            'url' => "https://bitfinex.com/"
        ]);
    }
}
