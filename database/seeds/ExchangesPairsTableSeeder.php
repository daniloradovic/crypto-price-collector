<?php

use Illuminate\Database\Seeder;

class ExchangesPairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '1',
            'pair_id' => '1'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '1',
            'pair_id' => '2'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '1',
            'pair_id' => '3'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '1',
            'pair_id' => '4'
        ]);

        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '2',
            'pair_id' => '1'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '2',
            'pair_id' => '2'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '2',
            'pair_id' => '3'
        ]);
        \App\Models\ExchangePair::firstOrCreate([
            'exchange_id' => '2',
            'pair_id' => '4'
        ]);
    }
}
