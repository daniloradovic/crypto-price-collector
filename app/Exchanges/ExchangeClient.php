<?php


namespace App\Exchanges;


use App\Models\Exchange;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

abstract class ExchangeClient extends WebSocketClient {

    protected $exchange;

    public function __construct()
    {
        $exchange = Exchange::whereCode(static::EXCHANGE_CODE)->first();
        $this->exchange = $exchange;
        parent::__construct();
    }

    public function config()
    {
        $this->server = static::WS_SERVER_URL;
    }

    /**
     * Subscribes for the channel. Abstract function as
     * every client needs to subscribe at least for one channel
     *
     */
    public function subscribe()
    {
        $pairs = $this->exchange->pairs->pluck('name');

        WebSocketClient::logToJournal(self::WS_ACTION_SUBSCRIBING, ['pairs' => $pairs, 'exchangeCode' => static::EXCHANGE_CODE]);
        foreach ($pairs as $pair)
        {
            $normalizedPair = $this->normalizePair($pair);
            $this->subscribeChannel(static::CHANNEL_TICKER, $normalizedPair);
        }
    }

    /**
     * Update the pair price based on ticker
     *
     * @param $pair
     * @param $message
     */
    protected function updateTicker($pair, $message)
    {
        $tickerData = $this->parseTickerMessage($message);
        $pairPrice = $tickerData['last_price'];

        $this->updatePairPrice($pair, $pairPrice);
    }

    /**
     * Update price in redis. If no price is passed, update just timestamp
     *
     * @param $pair
     * @param $pairPrice
     */
    protected function updatePairPrice($pair, $pairPrice = null)
    {
        $pairDataJson = Redis::hget('QOINPRO_PRICES', static::EXCHANGE_CODE);
        $exchangeData = json_decode($pairDataJson, true);

        $exchangeData[$pair]['timestamp'] = time();
        $exchangeData['timestamp'] = time();

        // If pairPrice is not passed, use old value.
        if (!empty($pairPrice))
        {
            $exchangeData[$pair]['price'] = $pairPrice;
        }

        Redis::hset('QOINPRO_PRICES', static::EXCHANGE_CODE, json_encode($exchangeData));
        Log::info(static::LOG_TAG. " Price update for $pair to $pairPrice");
    }

}