<?php

namespace App\Exchanges;

use App\Exchanges\GdaxPairs;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class GdaxClient extends ExchangeClient {

	const WS_SERVER_URL = "wss://ws-feed.gdax.com";
	const EXCHANGE_CODE = "GDAX";
	const LOG_TAG = "GDAX_WS";
	const CHANNEL_TICKER = "ticker";
    const CHANNEL_HEARTBEAT = "heartbeat";

	private $subscribedChannels = [];

    /**
     * Subscribes for ticker channel
     *
     * @param $channel
     * @param $pair
     * @throws \WebSocket\BadOpcodeException
     */
	public function subscribeChannel($channel, $pair) 
    {
		
		if ($channel == self::CHANNEL_TICKER) 
		{
			$requestMessage = $this->getTickerSubscribeRequest($pair);
			$this->send($requestMessage);
		}
	}

	/**
     * Generates ticker subscribe message
     *
     * @param $pair
     * @return string
     */
	public function getTickerSubscribeRequest($pair) 
    {
		
		$request = [
			'type' => 'subscribe',
			'product_ids' => [$pair],
			'channels' => [
				self::CHANNEL_TICKER,
			]
		];

		return json_encode($request);
	}

    // if needed
	private function unsubscribeChannel($pair, $channelType) 
    {

		if (empty($channelType))
		{
			Log::error(self::LOG_TAG. " Unsubsrbing error. There is no channel type for $pair");
			return;
		}

		$requestParams = [
			'type' => 'unsubscribe',
			'product_ids' => [$pair],
			'channels' => [$channelType]
 		];

 		$this->client->send($requestParams);
	}

	private function getSubscribedChannel($pair) 
    {


	}

	/**
     * Handles snapshots and updates messages
     *
     * @param $message
     * @return mixed
     * @throws \WebSocket\BadOpcodeException
     */
    public function messageHandler($message)
    {
        $messageDecoded = json_decode($message, true);
        $result = false;
		$responseType = $messageDecoded['type'];

        // If we got channel subscribed message, record it
        if ($responseType == 'subscriptions') 
        {
            $channels = array_column($messageDecoded['channels'], 'name');

            foreach ($channels as $channel)
            {
                Log::info(self::LOG_TAG. " Successfully subscribed to ". ucwords($channel));
            }

        	$this->handleSubscribedEvent($messageDecoded);
        }

        // check if message contains error
        if ($messageDecoded['type'] == 'error')
        {
            Log::error(self::LOG_TAG. " Unexpected message $message");
            return false;
        }

        if (!empty($messageDecoded['type']) and ($messageDecoded['type'] == 'heartbeat'))
        {   
            $this->handleHeartbeatMessage($messageDecoded);
            
            return;
        }

        // If we got message for one of subscribed channels
        if (!empty($responseType))
        {
            // Update ticker
            if ($responseType == self::CHANNEL_TICKER)
            {
                Log::info(self::LOG_TAG. ' New ticker message');
                $pair = $messageDecoded['product_id'];
                $this->updatePairPrice($pair, $messageDecoded['price']);
            }

            return;
        }

        Log::error(self::LOG_TAG. " Unknown WS message $message");

        return $result;
    }

    /**
     * Checks if price data is valid
     *
     * @param $pair
     * @return bool
     */
    private function isPriceDataValid($pair)
    {
        // TODO Implement Redis price validation logic
        return true;
    }
    // dammy method - not needed for this exchange
	public function eventHandler($message)
    {
        
    }
    // handler for Heartbeat channel messages - this channel needs manual subscription
    public function handleHeartbeatMessage($message)
    {
        $channelType = $message['type'];
        $pair = $message['product_id'];
        $last_trade_id = $message['last_trade_id'];
        $timestamp = $message['time'];
        
        Log::info(self::LOG_TAG. " Heartbeat message with last trade id $last_trade_id for pair - $pair at the time $timestamp");
    }

     /**
     * Handles ws subscribe event by storing: channel ID, pair and channel
     * Channel is not needed at the moment but maybe needed if we decided
     * to track more then one public channel
     *
     * @param $decodedMessage
     */
    private function handleSubscribedEvent($decodedMessage)
    {
        $channelCount = count($decodedMessage['channels']);

        for ($i = 0; $i<$channelCount; $i++){
            $channel = $decodedMessage['channels'][0]['name'];
            $pairs = $decodedMessage['channels'][0]['product_ids'];
            $pair = $this->denormalizePair($decodedMessage['channels'][$i]['product_ids'][0]);
        }

        Log::info(self::LOG_TAG." Subscribed to channel " . ucfirst($channel));
        $this::logToJournal(self::WS_ACTION_SUBSCRIBED, ['pair' => $pair, 'exchangeCode' => static::EXCHANGE_CODE]);

        $this->subscribedChannels[] = [
        	'channel' => $channel,
            'pairs' => $pairs
        ];
    }

    /**
     * Handles unsubscribe ws event with default behavior to reconnect after
     * unsubscribing
     *
     * @param $decodedMessage
     * @param bool $reconnect
     * @throws \WebSocket\BadOpcodeException
     */
    // private function handleUnsubscribedEvent($decodedMessage, $reconnect = true)
    // {
    //     $channelType = $decodedMessage['type'];
    //     if ($decodedMessage['status'] == 'OK')
    //     {
    //         // If channel is set, resubscribe it
    //         if (isset($this->subscribedChannels[$channelId]))
    //         {
    //             $channel = $this->subscribedChannels[$channelId]['channel'];
    //             $pair = $this->subscribedChannels[$channelId]['pair'];

    //             $this::logToJournal(self::WS_ACTION_UNSUBSCRIBED, ['pair' => $pair, 'exchangeCode' => static::EXCHANGE_CODE]);

    //             unset($this->subscribedChannels[$channelId]);

    //             if ($reconnect){
    //                 $this->subscribeChannel($channel, $pair);
    //             }
    //         }
    //     }
    //     else
    //     {
    //         Log::error(self::LOG_TAG. ' Error while unsubscribing from channel '. json_encode($decodedMessage));
    //     }
    // }

    /**
     * Converts system pair BTC_USD to bitfinex pair BTCUSD
     *
     * @param $pair
     * @return mixed
     */
    public function normalizePair($pair)
    {
        return str_replace('_','-', $pair);
    }

    /**
     * Convert exchange BTC-USD pair to systems BTC_USD pair
     *
     * @param $pair
     * @return mixed
     */
    public function denormalizePair($pair)
    {
        // $pair = ltrim($pair, 't');
        return str_replace("_","-",$pair);
    }

}
