<?php


namespace App\Exchanges;

use WebSocket\Client;
// use App\Models\WebsocketLog;
use Illuminate\Support\Facades\Log;

abstract class WebSocketClient {

    const WS_ACTION_CREATED = 'created';
    const WS_ACTION_SUBSCRIBING = 'subscribing';
    const WS_ACTION_SUBSCRIBED = 'subscribed';
    const WS_ACTION_UNSUBSCRIBING = 'unsubscribing';
    const WS_ACTION_UNSUBSCRIBED = 'unsubscribed';
    const WS_ACTION_RESTART = 'restarted';
    const WS_ACTION_KILL = 'killed';

    public static $logActions = [
        self::WS_ACTION_CREATED,
        self::WS_ACTION_SUBSCRIBING,
        self::WS_ACTION_SUBSCRIBED,
        self::WS_ACTION_UNSUBSCRIBING,
        self::WS_ACTION_UNSUBSCRIBED,
        self::WS_ACTION_RESTART,
        self::WS_ACTION_KILL
    ];

    protected $logTag = "WS";
    public $server;
    public $client;

    public function __construct()
    {
        $this->config();
        $this->client = new Client($this->server, ['timeout' => 60]);
        self::logToJournal(self::WS_ACTION_CREATED, ['exchangeCode' => static::EXCHANGE_CODE]);
        $this->subscribe();
    }

    /**
     * Basic WS configuration. Supposed to be overwritten by
     * implementing class
     *
     */
    protected function config()
    {
        $this->label = 'default';
        $this->server = 'wss://127.0.0.1/ws';
    }

    /**
     * Subscribes for the channel. Abstract function as
     * every client needs to subscribe at least for one channel
     *
     * @return bool
     */
    abstract function subscribe();


    /**
     * Starts receiving messages. Receives messages are
     * forwarded to router
     */
    public function run()
    {
        while (1) {
            $message = $this->client->receive();
            Log::info(get_called_class()::LOG_TAG. " New message arrived $message");
            $this->router($message);
        }
    }

    /**
     * Routes messages between different types of handlers
     *
     * @param $message
     */
    private function router($message)
    {

        // If message is event handle it
        if ($this->isEventMessage($message))
        {
            $this->eventHandler($message);
        }
        // Snapshots and updates handling
        else
        {
            $this->messageHandler($message);
        }

    }

    /**
     * Determines if a ws message is a event
     *
     * @param $message
     * @return bool
     */
    protected function isEventMessage($message)
    {
        return substr($message, 0, 8) == '{"event"';
    }
    /**
     * Handles snapshots and updates messages
     *
     * @param $message
     * @return mixed
     */
    abstract function messageHandler($message);

    /**
     * Handles event messages
     *
     * @param $message
     * @return mixed
     */
    abstract function eventHandler($message);

    /**
     * Sends message to ws server
     *
     * @param $message
     * @throws \WebSocket\BadOpcodeException
     */
    public function send($message)
    {
        $this->client->send($message);
    }

    /**
     * Logs websocket action to database
     *
     * @param $action
     * @param $params
     * @param null $wsPid
     * @return bool
     */
    public static function logToJournal($action, $params, $wsPid = null)
    {
        $paramsJson = json_encode($params);

        if (!in_array($action, self::$logActions))
        {
            Log::error("Unsupported web socket log action $action for $paramsJson");
            return false;
        }

        // in some cases killing is done by monitoring agent, not the process itself.
        // Consequently, pid is pass
        $pid = (empty($wsPid)) ? getmypid() : $wsPid;

        // WebsocketLog::create([
        //     'ws_process' => static::class,
        //     'pid' => $pid,
        //     'what' => $action,
        //     'data' => $paramsJson,
        //     'stamp' => time(),
        // ]);
    }

}