<?php 

namespace App\Exchanges;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class GdaxPairs 
{	

	public static $pairs = [
		"BCH-BTC",
  		"BCH-USD",
  		"BTC-EUR",
  		"BTC-GBP",
  		"BTC-USD",
  		"ETH-BTC",
  		"ETH-EUR",
  		"ETH-USD",
  		"LTC-BTC",
  		"LTC-EUR",
  		"LTC-USD"
	];

	public function getPairs() 
	{
		return $this::$pairs;
	}

	// public function getPairs() 
	// {
		
	// 	$apiUrl = "https://api.gdax.com/products";
		
	// 	$client = new Client();
		
	// 	$res = $client->request('GET', $apiUrl);
		
	// 	$resContents = $res->getBody()->getContents();

	// 	$resContentDecoded = json_decode($resContents);
		
	// 	for ($i=0; $i<count($resContentDecoded); $i++) {
	// 		$pairs[] = $resContentDecoded[$i]->id;
	// 	}		

	// 	dd($pairs);
	// }

}
