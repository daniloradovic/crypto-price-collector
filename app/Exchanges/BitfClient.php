<?php


namespace App\Exchanges;


use App\Models\Exchange;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class BitfClient extends ExchangeClient {

    const WS_SERVER_URL = "wss://api.bitfinex.com/ws/2";
    const EXCHANGE_CODE = "BITF";
    const LOG_TAG = "BITF_WS";
    const CHANNEL_TICKER = 'ticker';

    private $subscribedChannels = [];

    /**
     * Subscribes for ticker channel
     *
     * @param $channel
     * @param $pair
     * @throws \WebSocket\BadOpcodeException
     */
    protected function subscribeChannel($channel, $pair){

        if ($channel == self::CHANNEL_TICKER)
        {
            $requestMessage = $this->getTickerSubscribeRequest($pair);
            $this->send($requestMessage);
        }
    }

    /**
     * Generates ticker subscribe message
     *
     * @param $pair
     * @return string
     */
    protected function getTickerSubscribeRequest($pair)
    {
        $request =  [
            'event' => 'subscribe',
            'channel' => self::CHANNEL_TICKER,
            'symbol' => $pair
        ];

        return json_encode($request);
    }

    /**
     * Unsubscribe from channel
     *
     * @param $pair
     * @throws \WebSocket\BadOpcodeException
     */
    private function unsubscribeChannel($pair)
    {
        $channelId = $this->getSubscribedChannel($pair);

        if (empty($channelId))
        {
            Log::error(self::LOG_TAG. " Unsubscribing error. There is no channel id for $pair");
            return;
        }

        $requestParams = [
            "event" => "unsubscribe",
            "chanId" => $channelId
        ];
        $request = json_encode($requestParams);

        $this->client->send($request);
        Log::info(self::LOG_TAG. " Unsubsrbing from: $pair");
    }

    /**
     * Get subscribed channel for the given pair
     *
     * @param $pair
     * @return int|null|string
     */
    private function getSubscribedChannel($pair){

        foreach ($this->subscribedChannels as $channelId => $channelData)
        {
            if (in_array($pair, $channelData)){
                return $channelId;
            }
        }

        return null;
    }

    /**
     * Handles snapshots and updates messages
     *
     * @param $message
     * @return mixed
     * @throws \WebSocket\BadOpcodeException
     */
    public function messageHandler($message)
    {
        $messageDecoded = json_decode($message, true);

        $result = false;

        if (empty($messageDecoded[0]))
        {
            Log::error(self::LOG_TAG. " Unexpected message $message");
            return false;
        }

        $channelId = $messageDecoded[0];
        Log::info(self::LOG_TAG. " WS message on channel $channelId");

        if (!empty($messageDecoded[1]) and ($messageDecoded[1] == 'hb'))
        {
            $this->handleHeartbeatMessage($channelId);
            return;
        }

        // If we got message for one of subscribed channels
        if (!empty($this->subscribedChannels[$channelId]))
        {
            $channel = $this->subscribedChannels[$channelId]['channel'];

            // Update ticker
            if ($channel == self::CHANNEL_TICKER)
            {
                Log::info(self::LOG_TAG. ' New ticker message');
                $pair = $this->subscribedChannels[$channelId]['pair'];
                $this->updateTicker($pair, $messageDecoded[1]);
            }

            return;
        }

        Log::error(self::LOG_TAG. " Unknown WS message $message");

        return $result;
    }

    /**
     * Touch the pair price data
     *
     * @param $channelId
     * @throws \WebSocket\BadOpcodeException
     */
    public function handleHeartbeatMessage($channelId)
    {
        $channelData = $this->subscribedChannels[$channelId];
        $pair = $channelData['pair'];
        $channel = $channelData['channel'];

        // If channel is ticker
        if ($channel == self::CHANNEL_TICKER)
        {
            // Check if price data is valid. If yes update the price.
            // If no resubscribe)
            if ($this->isPriceDataValid($pair))
            {
                $this->updatePairPrice($pair);
            }
            else {
                // unsubscribing automatically triggers resubscription
                $this->unsubscribeChannel($pair);
            }
        }

    }

    /**
     * Checks if price data is valid
     *
     * @param $pair
     * @return bool
     */
    private function isPriceDataValid($pair)
    {
        // TODO Implement Redis price validation logic
        return true;
    }

    /**
     * Parse ticker message and return associative array
     *
     * @param $message
     * @return array
     */
    protected function parseTickerMessage($message)
    {
        $data = [
            'bid' => empty($message[0]) ? null : $message[0],
            'bid_size' => empty($message[1]) ? null : $message[1],
            'ask' => empty($message[2]) ? null : $message[2],
            'ask_size' => empty($message[3]) ? null : $message[3],
            'daily_change' => empty($message[4]) ? null : $message[4],
            'daily_change_perc' => empty($message[5]) ? null : $message[5],
            'last_price' => empty($message[6]) ? null : $message[6],
            'volume' => empty($message[7]) ? null : $message[7],
            'high' => empty($message[8]) ? null : $message[8],
            'low' => empty($message[9]) ? null : $message[9],
        ];

        return $data;
    }

    /**
     * Handles event messages
     *
     * @param $message
     * @return mixed
     * @throws \WebSocket\BadOpcodeException
     */
    public function eventHandler($message)
    {
        $result = json_decode($message, true);

        $event = $result['event'];

        if ($event == 'error')
        {
            Log::error(self::LOG_TAG." Error in event handling - ".$result['msg']);
            return;
        }

        // If we got channel subscribed message, record it
        if ($event == 'subscribed')
        {
            $this->handleSubscribedEvent($result);
            return;
        }

        if ($event == 'unsubscribed')
        {
            $this->handleUnsubscribedEvent($result);
            return;
        }
    }

    /**
     * Handles ws subscribe event by storing: channel ID, pair and channel
     * Channel is not needed at the moment but maybe needed if we decided
     * to track more then one public channel
     *
     * @param $decodedMessage
     */
    private function handleSubscribedEvent($decodedMessage)
    {
        $channelId = $decodedMessage['chanId'];
        $channel = $decodedMessage['channel'];
        $pair = $this->denormalizePair($decodedMessage['pair']);

        Log::info(self::LOG_TAG." Subscribed to channel id $channelId");
        $this::logToJournal(self::WS_ACTION_SUBSCRIBED, ['pair' => $pair, 'exchangeCode' => static::EXCHANGE_CODE]);

        $this->subscribedChannels[$channelId] = [
            'pair' => $pair,
            'channel' => $channel
        ];
    }

    /**
     * Handles unsubscribe ws event with default behavior to reconnect after
     * unsubscribing
     *
     * @param $decodedMessage
     * @param bool $reconnect
     * @throws \WebSocket\BadOpcodeException
     */
    private function handleUnsubscribedEvent($decodedMessage, $reconnect = true)
    {
        $channelId = $decodedMessage['chanId'];
        if ($decodedMessage['status'] == 'OK')
        {
            // If channel is set, resubscribe it
            if (isset($this->subscribedChannels[$channelId]))
            {
                $channel = $this->subscribedChannels[$channelId]['channel'];
                $pair = $this->subscribedChannels[$channelId]['pair'];

                $this::logToJournal(self::WS_ACTION_UNSUBSCRIBED, ['pair' => $pair, 'exchangeCode' => static::EXCHANGE_CODE]);

                unset($this->subscribedChannels[$channelId]);

                if ($reconnect){
                    $this->subscribeChannel($channel, $pair);
                }
            }
        }
        else
        {
            Log::error(self::LOG_TAG. ' Error while unsubscribing from channel '. json_encode($decodedMessage));
        }
    }

    /**
     * Converts system pair BTC_USD to bitfinex pair BTCUSD
     *
     * @param $pair
     * @return mixed
     */
    public function normalizePair($pair)
    {
        return str_replace('_','', $pair);
    }

    /**
     * Convert exchange BTCUSD pair to systems BTC_USD pair
     *
     * @param $pair
     * @return mixed
     */
    public function denormalizePair($pair)
    {
        $pair = ltrim($pair, 't');
        return substr_replace($pair, '_', 3, 0);
    }

}