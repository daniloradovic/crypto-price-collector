<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangePair extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exchange_id', 'pair_id',
    ];

	/**
     * Get exchange associated with thie exchangePair
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exchange()
    {
        return $this->belongsTo(Exchange::class);
    }

	/**
     * Get pair associated with this exchangePair
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pair()
    {
        return $this->belongsTo(Pair::class);
    }

}
