<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'country',
    ];

	/**
     * Returns pairs associated with the exchange
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pairs()
    {
        return $this->belongsToMany(Pair::class, 'exchange_pairs');
    }

	/**
     * Return exchange pairs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exchangePairs()
    {
        return $this->hasMany(ExchangePair::class);
    }
}
