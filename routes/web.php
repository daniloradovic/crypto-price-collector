<?php

use WebSocket\Client;
use App\Exchanges\GdaxClient;
use Illuminate\Support\Facades\Redis;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('articles/trending', function(){
	$trending = Redis::zrevrange('trending_articles', 0, 2, 'WITHSCORES');

	return $trending;
});

$router->get('articles/{id}', function($id){
	Redis::zincrby('trending_articles', 1, 'article' . $id);

	return 'article' . $id;
});

$router->get('/', function() {

	return Cache::remember('articles.all', 10, function() {
		$articles = [
			'article 1' => 'first article',
			'article 2' => 'second article',
			'article 3' => 'third article',
			'article 4' => 'fourth article',
			'article 5' => 'fifth article',
			'article 6' => 'sixth article',
			'article 7' => 'seventh article',
			'article 8' => 'eight article',
			'article 9' => 'nineth article',
			'article 10' => 'tenth article',
		];

		return json_encode($articles);
	});
});

// $router->get('/socket', function() {
// 	$gdaxClient = new GdaxClient();
// 	return $gdaxClient->run();
// });